<html>
<head>
    <?php wp_head(); ?>
    <link rel="stylesheet" href="https://use.typekit.net/ebt8gve.css">
    <link rel="stylesheet" type="text/css" href="https://cloud.typography.com/7203196/6234572/css/fonts.css">
</head>
<body>
<h2 style="text-align: center;">Rubric</h2>

<?php if ( ! is_user_logged_in() ) : ?>
    <h2>You must be logged in to use the Rubric. Please <a href="/wp-login.php">Login</a></h2>
<?php else : ?>
<div id="rubric">
    <div class="loader" v-if="loader">Loading...</div>
    <div class="no-questions-message" v-if="(!loader && !report && !questions.length)">
        {{ noQuestionsMessage }}
    </div>
    <div class="questions" v-if="(!loader && !report && questions.length )">
        <input type="text" v-model="title" placeholder="Business Name" />
        <div v-for="(obj, index) in questions" class="question" :id="`q${index}`">
            <blockquote>{{ obj.text }}</blockquote>
            <label>Yes <input type="radio" value="yes" :name="`q${index}`" @change="addAnswer(index, 'yes')" /></label>
            <label>No <input type="radio" value="no" :name="`q${index}`" @change="addAnswer(index, 'no')" /></label>
            <label v-if="obj.require_image_upload" >Screenshot (3MB Limit) <input id="fileupload" type="file" accept="image/*" @change="addFile($event, index)" /></label>
            <label v-if="obj.require_url"><input placeholder="http://example.com or https://www.youtube.com/watch?v=X_v_kJsi7TY" type="url" @change="addUrl(index, $event)" /></label>
            <div class="error upload-error">File size too large.</div>
        </div>
        <div v-if="uploadingFiles.length"><span class="loader inline"></span> Uploading Files - Please Wait</div>
        <button id="submit" @click="generateReport" :disabled="isUploadingFiles">Submit</button>
    </div>
    <div v-if="(showErrorMessage && !loader)">{{ errorMessage }} - <a href="#" @click="clearError">Dismiss</a></div>
    <div v-if="saveResponse"><p><a class="button" target="_blank" :href="reportUrl">View Report</a></p></div>
    <div class="report" v-html="report"></div>
</div>
<?php endif; ?>

<?php wp_footer(); ?>
</body>
</html>