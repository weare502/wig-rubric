<?php

/**
 * Plugin Name: WIG Rubric
 * Description: Enables the client evaluation form. Ideas from Creative team WIG Retreat March 2018
 * Author: 502 - A Strategic Marketing Agency
 */

add_action('init', function(){
    global $wp_rewrite;
    //set up our query variable %rubric% which equates to index.php?rubric= 
    add_rewrite_tag( '%rubric%', '([^&]+)'); 
    //add rewrite rule that matches /rubric
    add_rewrite_rule('^rubric/?','index.php?rubric=true','top');
    //add endpoint, in this case 'rubric' to satisfy our rewrite rule /test
    add_rewrite_endpoint( 'rubric', EP_PERMALINK | EP_PAGES );
});

add_action('acf/init', function(){
    acf_add_options_page(array(
		'page_title' 	=> 'WIG Rubric',
		'menu_title'	=> 'WIG Rubric',
		'menu_slug' 	=> 'wig-rubric',
		'capability'	=> 'edit_posts',
        'redirect'		=> false,
	));
});

add_filter('template_include', function($template){

    if ( untrailingslashit( $_SERVER['REQUEST_URI'] ) === '/rubric' ){
        $template = plugin_dir_path( __FILE__ ) . 'view.php';
    }

    return $template;

}, 99 );

add_action('wp', function(){
    global $wp;
    if ( array_key_exists( 'rubric', $wp->query_vars) ){
        wp_enqueue_script( 'wp-api' );
        wp_enqueue_style( 'rubric', plugin_dir_url( __FILE__ ) . 'rubric.css', array(), '20180303' );
        wp_enqueue_script( 'vue-rubric', plugin_dir_url( __FILE__ ) . 'vue.js', array('jquery'), '20180301', true );
        wp_enqueue_script( 'rubric', plugin_dir_url( __FILE__ ) . 'rubric.js', array('jquery', 'vue-rubric'), '20180304', true );
    }
});

add_action( 'plugins_loaded', function(){
    if ( ! isset( $_REQUEST['rubric-image-upload'] ) )
        return;

    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );
    
    //On success it returns attachment ID
    $attachment_id = media_handle_upload('image', 0);
    
    echo wp_get_attachment_image_url($attachment_id, 'large');
    
    die();

} );

add_action('init', function(){
    register_post_type('rubric', array(
        'public' => true,
        'label' => "Rubrics",
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'show_in_admin_bar' => false,
        'supports' => array('title', 'editor'),
        'rewrite' => array('slug' => 'reviews'),
        'query_var' => false
    ));
});

add_filter('acf/settings/load_json', function($paths){
    $paths[] = plugin_dir_path( __FILE__ ) . 'acf-json';
    return $paths;
});

add_filter('post_type_link', function( $link, $post ){
    if ( $post->post_type === 'rubric' ){
        $encoded = base64_encode( home_url() . '/wp-json/wp/v2/rubric/' . $post->ID );
        $link = 'https://weare502.com/assessments/' . $encoded;
    }
    return $link;
}, 10, 2);