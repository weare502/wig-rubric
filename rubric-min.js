/* global Vue, wp */

var rubric = new Vue({ // eslint-disable-line no-unused-vars
    el: '#rubric',
    data: {
        title: '',
        form: [],
        questions: [],
        userAnswers: [],
        uploadingFiles: [],
        loader: false,
        report: '',
        errorMessage: '',
        showErrorMessage: false,
        saveResponse: false,
        noQuestionsMessage: 'It appears there are no questions to answer yet. Login to wp-admin and add some under the "WIG Rubric" section.'
    },
    methods: {
        addAnswer: function(index, answer){
            this.userAnswers[index] = Object.assign({}, this.userAnswers[index] || {}, {answer: answer} );
        },
        addFile( event, index ){
            var field = event.target;
            var input = jQuery( field );
            input.closest('.question').find('.upload-error').hide();
            this.userAnswers[index] = Object.assign({}, this.userAnswers[index] || {}, {file: null} );
            if ( event.target.files[0].size > 3145728 ){
                input.closest('.question').find('.upload-error').show();
                // clear the uploaded image
                input.val('');
                this.userAnswers[index] = Object.assign({}, this.userAnswers[index] || {}, {file: null} );
                return;
            }
            this.uploadingFiles.push(index);
            var data = new FormData();
            var self = this;
            data.append('image', event.target.files[0]);
            jQuery.ajax({
                url: '/?rubric-image-upload',
                data: data,
                method: 'POST',
                processData: false,  // tell jQuery not to process the data
                contentType: false,
                success: function(data){
                    self.userAnswers[index] = Object.assign({}, self.userAnswers[index] || {}, {file: data} );
                    self.uploadingFiles.pop(); // remove 1 item from the uploading files array
                }
            });
        },
        addUrl(index, e){
            let url = e.target.value;
            this.userAnswers[index] = Object.assign({}, this.userAnswers[index] || {}, {url: url} );
        },
        generateReport: function(){
            this.loader = true;

            if ( this.userAnswers.length !== this.questions.length ){
                this.error('All answers are required.');
                this.loader = false;
                return;
            }

            this.report = this.concatenateMessages();
            
            this.clearError();
            this.loader = false;
            
            var self = this;
            var post = new wp.api.models.Rubric({
                title: self.title,
                content: self.report,
                status: 'publish'
            });
            post.save( null, {
                success: function(model, response) {
                    console.log(response);
                    self.saveResponse = response;
                },
                error: function(model, response) {
                    console.log(response);
                }
            });
        },
        concatenateMessages: function(){
            var message = '';
            var tab_open = false;

            // Build Form Layout
            this.form.forEach(field => {
                if ( field.acf_fc_layout === "tab_start" ){
                    if ( tab_open ){
                        message += "</div><!-- tab-end -->";
                    }
                    message += `<div class="tab-start" data-tab-title="${field.tab_title}">`;
                    tab_open = true;
                }

                if ( field.acf_fc_layout === "headline" ){
                    message += `<h2>${field.headline}</h2>`;
                }

                if ( field.acf_fc_layout === "question" ){
                    var questionIndex = this.questions.indexOf(field);
                    var userAnswer = this.userAnswers[questionIndex];
                    const rawAnswer = field[userAnswer.answer];
                    const formattedAnswer = rawAnswer.replace('%url%', `<a href="${userAnswer.url}">${userAnswer.url}</a>`);
                    message += `<p>${formattedAnswer}</p>`;
                    if ( typeof userAnswer.file !== 'undefined' ){
                        message += `<p><img src='${userAnswer.file}' /></p>`;
                    }
                }
            });

            // Close open tab
            if ( tab_open ){
                message += "</div><!-- tab-end -->";
                tab_open = false;
            }

            return message;
        },
        error: function(message){
            this.errorMessage = message;
            this.showErrorMessage = true;
        },
        clearError: function(){
            this.errorMessage = '';
            this.showErrorMessage = false;
        }
    },
    computed: {
        isUploadingFiles: function(){
            return this.uploadingFiles.length ? true : false;
        },
        reportUrl: function(){
            if ( this.saveResponse ){
                let encoded = btoa(`https://tools.weare502.com/wp-json/wp/v2/rubric/${this.saveResponse.id}`);
                return `https://weare502.com/assessments/${encoded}`;
            } else {
                return "";
            }
        }
    },
    mounted: function(){
        this.loader = true;
        var self = this;
        jQuery.ajax({
            url: '/wp-json/acf/v3/options/options',
            method: 'GET',
            success: function( data ){
                console.log("data retrieved");
                self.form = data.acf.rubric_form;

                self.form.forEach(field => {
                    if ( field.acf_fc_layout === "question" ){
                        self.questions.push(field);
                    }
                });
                self.loader = false;
            },
            error: error => alert(error)
        });
    }
});

